# Create your tests here.
from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse
from guestbook.models import GuestBook

test_data = {"name": "Deniz",
             "subject": "Some Subject",
             "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consectetur sed mi vitae vehicula. Nullam venenatis odio ac efficitur elementum. Etiam at orci augue. Suspendisse ultricies sagittis nulla at ultrices. Sed nulla ex, aliquet nec vehicula sed, sagittis id sapien. Quisque lorem velit, scelerisque ut maximus eu, pharetra at massa. Donec tristique mauris ac libero accumsan fermentum. In vitae magna eget velit feugiat sodales a in mauris.\
                        Vestibulum dictum magna quis odio blandit imperdiet. In consectetur nunc a erat posuere ullamcorper. Integer vestibulum facilisis enim, vitae ullamcorper justo mattis et. Nunc efficitur eros justo, ac accumsan augue feugiat eu. Vestibulum eleifend placerat nisl, vitae consequat risus accumsan non. Phasellus pharetra pharetra ante id consequat. Praesent id dapibus ante, sit amet efficitur nisl. Maecenas ultricies auctor tellus. Vestibulum eget felis non nunc gravida accumsan. Cras tristique pharetra neque quis dictum. Fusce luctus purus ac enim vulputate, hendrerit consequat lectus malesuada. Vestibulum a ipsum at nulla faucibus feugiat. Duis placerat est vel nisl congue, iaculis scelerisque odio viverra. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse lorem elit, dictum nec lacus interdum, lacinia fermentum enim.\
                        Sed efficitur sagittis aliquam. Curabitur id magna vulputate, sodales sapien at, convallis orci. Nulla at dapibus ipsum. Vivamus tincidunt quam vitae luctus lobortis. In tristique congue nisl non varius. Nulla tincidunt ullamcorper euismod. Nam mi erat, rhoncus ultrices eros a, sagittis laoreet ligula. Phasellus ac tortor non justo pellentesque congue. Donec mattis ipsum ac ligula pellentesque, ut sagittis odio iaculis. Pellentesque massa sapien, pellentesque ut nisl vel, ornare dignissim mauris. Aliquam aliquet diam ac sem sodales, ut fermentum lectus porta. Ut vestibulum luctus velit, non vulputate est lacinia quis. Sed pharetra augue quis mauris tincidunt, in tincidunt justo ornare. Sed at purus a metus consectetur egestas."
             }


class TestCase(APITestCase):

    def test_post(self):
        url = reverse('guestbook')

        response = self.client.post(url, test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(GuestBook.objects.count(), 1)
        self.assertEqual(GuestBook.objects.get().name, 'Deniz')

    def test_get(self):
        url = reverse('root')
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
