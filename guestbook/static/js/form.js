function AddReview(resultSet, refresh) {

    for (let i = 0; i < resultSet.length; i++) {
        let user = resultSet[i].name;
        let date = new Date(resultSet[i].created).toLocaleString();
        let message = resultSet[i].message;

        let htmlElement = '<li>' +
            '<div>' +
            '<div style="float:left">' + user + '</div>' +
            '<div style="float:right;text-align: right">' + date + '</div>' +
            '<div style="float:none;padding-top: 40px;text-align: justify">' + message + '</div>' +
            '</div>' +
            '</li>'

        if (refresh) {
            $('#reviews').prepend(htmlElement);
            $('#success').css({display: 'block'});
            $('#success').fadeIn('slow').delay(1000).fadeOut('slow')
        } else {
            $('#reviews').append(htmlElement);
        }

    }
}

$(document).on('click', '#btn_send', function (e) {
    e.preventDefault();

    function objectifyForm(formArray) {
        const returnArray = {};
        for (let i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }

    const data = JSON.stringify(
        objectifyForm($('#guest_form').serializeArray())
    );

    $.ajax({
        type: "POST",
        url: "/guestbook/",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (returned_data) {
            AddReview([returned_data], true);
        },
        error: function (err, textStatus, errorThrown) {
            var ErrResult = '';
                $.each(JSON.parse(err.responseText), function(key, value) {
                    ErrResult += key.toUpperCase()  + ' : ' + value + "</br>";
                });
            $('#failed').html(errorThrown + " <br/><br/> " + ErrResult);
            $('#failed').css({display: 'block'});
            $('#failed').fadeIn('slow').delay(5000).fadeOut('slow')
        }
    });
});

$(document).ready(function () {

        $.ajax({
            type: "GET",
            url: "/guestbook/",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (returned_data) {
                AddReview(returned_data, false);
            }
        });
    }
)