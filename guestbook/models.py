from django.db import models


class GuestBook(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=False, default='')
    subject = models.CharField(max_length=256, blank=True, default='')
    message = models.TextField(blank=False)

    class Meta:
        ordering = ['-created']  # ordering model by new to old by default.

    def __str__(self):
        return self.name
