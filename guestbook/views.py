from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from guestbook.models import GuestBook
from guestbook.serializers import GuestBookSerializer
from django.template import loader


@csrf_exempt  # this is not pretty. we should never disable the csrf.
def guest_book_list(request):
    if request.method == 'GET':
        guest_opinions = GuestBook.objects.all()
        serializer = GuestBookSerializer(guest_opinions, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = GuestBookSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


def guest_book_interface(request):
    if request.method == 'GET':
        # we can actually get the data from here and use the Django's basic interface templates.
        # but we should use the rest api to get the guest book resultset as requested.
        # so this view does nothing but just display the template. then javascript take cares the rest.
        template = loader.get_template('GuestBook.html')
        return HttpResponse(template.render())
