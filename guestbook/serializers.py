from rest_framework import serializers
from guestbook.models import GuestBook
from rest_framework.validators import UniqueTogetherValidator


class GuestBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuestBook
        fields = ['id', 'name', 'subject', 'message', 'created']

        validators = [
            UniqueTogetherValidator(
                queryset=GuestBook.objects.all(),
                fields=['name', 'message']
            )
        ]
